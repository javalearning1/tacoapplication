package com.tendecoders.taco;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/design")
public class DesignTacoController {
	
	private static final Logger log = LoggerFactory.getLogger(DesignTacoController.class);
	
	@GetMapping
	public String showDesignForm (Model model) {
		List<Ingredient> ingredients = Arrays.asList(
				new Ingredient("MGTO", "Multigrain Tortilla", Ingredient.Type.WRAP),
				new Ingredient("COTO", "Corn Tortilla", Ingredient.Type.WRAP),
                new Ingredient("TOFU", "TOFU", Ingredient.Type.PROTEIN),
                new Ingredient("BEANS", "Fresh Beans", Ingredient.Type.PROTEIN),
                new Ingredient("LETT", "Lettuce", Ingredient.Type.VEGGIES),
                new Ingredient("TOMA", "Diced Tomatoes", Ingredient.Type.VEGGIES),
                new Ingredient("MONT", "Monterry Jack", Ingredient.Type.CHEESE),
                new Ingredient("CHED", "Cheddar", Ingredient.Type.CHEESE),
                new Ingredient("SOUR", "Sour Cream", Ingredient.Type.SAUCE),
                new Ingredient("SALS", "Salsa", Ingredient.Type.SAUCE));
		
		Ingredient.Type[] types= Ingredient.Type.values();
		for(Ingredient.Type type:types) {
			model.addAttribute(type.toString().toLowerCase(), filterByType(ingredients,type));
			
		}
		model.addAttribute("design", new Taco());
		return "design";
	}
	
	private List<Ingredient> filterByType(List<Ingredient> ingredients, Ingredient.Type type) {
		return ingredients
				.stream()
				.filter(x -> x.getType().equals(type))
				.collect(Collectors.toList());
	}
	
	@PostMapping
	public String processDesign(@Valid Taco design, Errors errors,Model model) {
		model.addAttribute("design",design);
		log.info("Processing Design: "+design);
		
		if(errors.hasErrors()) {
			return "design";
		}
		log.info("Processing Design Completed successfully: "+design);
		return "redirect:/orders/current";
	}
	
	

}
